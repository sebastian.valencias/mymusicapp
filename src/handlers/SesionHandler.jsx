

const loginState = function () { };





export const TokenManager = (param) => {

    const paramsInArray = param.substring(1).split('&');
    const tokenInfo = paramsInArray.reduce((accumulater, currentValue) => {
        const [key, value] = currentValue.split('=');
        accumulater[key] = value;
        return accumulater;
    }, {});

    localStorage.setItem('accessToken', (tokenInfo["token_type"] + ' ' + tokenInfo["access_token"]));
    localStorage.setItem('expiresIn', (Date.now() + (tokenInfo["expires_in"] * 1000)));

    window.location = (window.location + '').split('#')[0];
}

export const sesionExpiration = async (setloginState, timeToExpire) => {
    setTimeout(() => {
        localStorage.clear();
        setloginState(false);
    }, timeToExpire);
}

export const logOutHandler = () => {
    localStorage.clear();
    window.location = window.location;
}