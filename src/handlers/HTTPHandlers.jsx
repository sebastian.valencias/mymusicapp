import axios from "axios";
import { AuthReducer as authVAR } from "../reducers/AuthReducer";
import { store } from "../store/store";


//TODO hide this in ref  --|
const __CLIENT_ID = "353dbaae81ef4b899d072d61c2b5c048";
const __SCOPES = ["user-read-private", "user-follow-modify", "user-library-read", "user-library-modify", "playlist-modify-public"];
const __REDIRECTION_URI = "http://localhost:3000";
const __EXPIRATION = 3600;
//const __RESPONSE_TYPE="token";
//   |--


export const loginHandler = async () => {

    const url = "https://accounts.spotify.com/authorize?"
        + `client_id=${encodeURI(__CLIENT_ID)}`
        + `&scope=${encodeURI(__SCOPES.join('%20'))}`
        + `&redirect_uri=${encodeURI(__REDIRECTION_URI)}`
        + `&expires_in=${encodeURI(__EXPIRATION)}`
        + "&response_type=token&show_dialog=true";

    window.location = url;
}


export const profileDataHandler = (setUser) => {

    const baseUrl = "https://api.spotify.com/v1/me";
    axios.get(baseUrl, {
        headers: {
            Authorization: localStorage.getItem('accessToken')
        }

    }).then(({ data }) => {
        const userData = {
            name: data.display_name,
            image: data.images[0]?.url,
            uri: (data.uri).replace('spotify:user:', '')
        }
        setUser(userData)
    });
}

export const BrowseListsHandlerd = (setCategoryList) => {
    const baseUrl = `https://api.spotify.com/v1/browse/featured-playlists`;
    axios.get(baseUrl, {
        headers: {
            Authorization: localStorage.getItem('accessToken')
        },
        params: {
            country: 'CO',
            limit: 12,
            offset: 0
        }
    }).then(({ data }) => {
        console.log(data);
        const lists = data.playlists.items;
        setCategoryList(lists);
        return 1;


    });
}

export const favoritesHandler = (setFavoriteList) => {

    console.log("HOLAAAAAAAAAAA", store);
    console.log("llamado a FAVHANDLER");
    const baseUrl = `https://api.spotify.com/v1/me/tracks`;
    const response = axios.get(baseUrl, {
        headers: {
            Authorization: localStorage.getItem('accessToken')
        },
        params: {
            limit: 50,
            offset: 0
        }
    });
    return response;

}

export const getListHandler = (setList, idList) => {

    const baseUrl = `https://api.spotify.com/v1/playlists/${idList}`;

    const list = axios.get(baseUrl, {
        headers: {
            Authorization: localStorage.getItem('accessToken')
        }

    }).then(({ data }) => {
        console.log(data, "DATA DE GETLISTHANDLER");
        const songList = data.tracks.items.map((element) => {
            const { track } = element;
            const elementListProperties = {
                album: track.album.name,
                id: track.id,
                duration: track.duration_ms,
                name: track.name,
                image: track.album.images[0].url,
                saved: false,
                artist: (track.artists.map((trackartist) => {
                    return trackartist.name;
                })).join(', '),

            }

            return elementListProperties;
        });


        const ids = songList.map(({ id }) => { return id });
        create50SongsGroups(ids).then((savedSongArray) => {
            savedSongArray.map((element, index) => {
                songList[index].saved = element;
            })
            setList(songList);
        });

    });
}


const create50SongsGroups = async (completeArray) => {

    const checkSavedUrl = "https://api.spotify.com/v1/me/tracks/contains";

    let remainingElements = completeArray.length;
    let currentElement = 0;
    let groupArray = [];
    let savedSongArray = [];
    let count = 0;
    while (remainingElements > 0) {
        console.warn("holaaaa", remainingElements, '\n', completeArray.length);
        if (remainingElements < 50) {
            groupArray[count] = completeArray.slice(currentElement, completeArray.length);
            remainingElements = 0;
            count++;
        }
        else {
            groupArray[count] = completeArray.slice(currentElement, (currentElement + 50));
            currentElement += 50;
            remainingElements -= 50;
            count++;
        }
    }
    for (let i = 0; i < count; i++) {
        const body = `?ids=${groupArray[i].join(',')}`;
        //console.log("ESTO ES IMPORTANTE \n \n", stringList);
        let response = await axios.get(checkSavedUrl + '' + body, {
            headers: {
                Authorization: localStorage.getItem('accessToken')
            }

        }
        )


        savedSongArray = savedSongArray.concat(response.data);
    }
    return savedSongArray;
}

export const getUserListsHandler = (setList) => {

    const baseUrl = `https://api.spotify.com/v1/me/playlists`;
    axios.get(baseUrl, {
        headers: {
            Authorization: localStorage.getItem('accessToken')
        }
    }).then(({ data }) => {
        console.log(data, "llamada userlisthandler");
        const lists = data.items;
        setList(lists);
        return 1;
    });

}


export const saveSong = (songId) => {

    const baseUrl = 'https://api.spotify.com/v1/me/tracks';
    const requestOptions = {
        method: 'PUT',
        headers: { Authorization: localStorage.getItem('accessToken'), 'Content-Type': 'application/json' },
        body: JSON.stringify({ ids: [songId] })
    };

    fetch(baseUrl, requestOptions).then(() => {
        console.log("yeiiiiiiiiiiiii guardé");
    }).catch((e) => {
        console.error("como un demonio AGREGAR, lo que faltaba \n", e)
    });/*
    
    por algun estupido motivo este put con axios siempre devuelve un 401

    */

}



export const removeSavedSong = (songId) => {

    const baseUrl = `https://api.spotify.com/v1/me/tracks`;
    axios.delete(baseUrl, {
        headers: {
            Authorization: localStorage.getItem('accessToken')
        },
        params: {
            ids: songId
        }

    }).then(() => {
    }).catch((e) => {
        console.error("como un demonio ELIMINE, lo que faltaba \n", e)
    });
}


