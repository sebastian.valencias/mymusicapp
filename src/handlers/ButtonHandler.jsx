import { logOutHandler } from "./SesionHandler";

export const buttonSidebarClick = (actionDefinition) => {
    switch (actionDefinition.actionId) {
        case "log-out":
            logOutHandler();
            break;
        case "replace-class":
            const element = document.querySelector(actionDefinition.selector);
            element.classList.replace(actionDefinition.oldClass, actionDefinition.newClass);
            break;
            break;
        default: ;
    }
}
