import { UserStateType } from "../types/UserStateType"


export const login = (token, expiresIn) => {
    return {
        type: UserStateType.login,
        payload: {
            token,
            expiresIn
        }
    }
}