import React, { useEffect } from 'react';
import { AppRouter } from './routers/AppRouter';

import { Provider } from 'react-redux';
import { store } from './store/store';
import { TokenManager } from './handlers/SesionHandler';

function AppSpotify() {
    useEffect(() => {
        if (window.location.hash) {
            TokenManager(window.location.hash);
        }

    }, [])


    return (
        <Provider store={store}>
            <AppRouter />
        </Provider>
    );
}


export default AppSpotify;
