
export const UserStateType = {
    login: '[Auth] Login',
    logout: '[Auth] Logout'
}