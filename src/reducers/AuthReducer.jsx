import { UserStateType } from "../types/UserStateType";

export const AuthReducer = (state = {}, action) => {
    switch (action.type) {
        case UserStateType.login:
            return {
                token: action.payload.token,
                expiresIn: action.payload.expiresIn
            }
        case UserStateType.logout:
            return {}
        default:
            return state;
    }
};