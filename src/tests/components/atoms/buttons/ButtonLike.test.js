//import { render } from "@testing-library/react";
import ButtonGeneral from "../../../../components/atoms/buttons/ButtonLike";
import { shallow } from 'enzyme';


import '@testing-library/jest-dom';
import ButtonLike from "../../../../components/atoms/buttons/ButtonLike";


/*
jest.mock("react-router-dom", () => ({
    ...jest.requireActual("react-router-dom"),
    useLocation: () => ({
        pathname: "localhost:3000/"
    })
}));*/

describe('GeneralLike', () => {
    //action, bordered = false, text, father, buttonType = "standar", specialBehaviour
    const params = [
        ['1234', false],
        ['5678', true]
    ]

    test('prueba renderizado', () => {
        let wrapper = shallow(<ButtonLike idElement={params[0][0]} saved={params[0][1]} />);
        expect(wrapper).toMatchSnapshot();
    });

    test('prueba clases', () => {
        for (let i = 0; i < params.length; i++) {
            let wrapper = shallow(<ButtonLike idElement={params[i][0]} saved={params[i][1]} />);
            expect(wrapper.hasClass('saved-song')).toEqual(params[i][1]);
        }
    });


})