//import { render } from "@testing-library/react";
import ButtonGeneral from "../../../../components/atoms/buttons/ButtonGeneral";
import { shallow } from 'enzyme';


import '@testing-library/jest-dom';

const numberOfTests = 5;



jest.mock("react-router-dom", () => ({
    ...jest.requireActual("react-router-dom"),
    useLocation: () => ({
        pathname: "localhost:3000/"
    })
}));

describe('GeneralButton', () => {
    //action, bordered = false, text, father, buttonType = "standar", specialBehaviour
    const actions = [
        ['Inicio', { actionId: 'redirect', destiny: '/' }]
    ]

    test('prueba renderizado', () => {
        let wrapper = shallow(<ButtonGeneral key={`sidebar-button-1`} action={actions[0][1]} text={actions[0][0]} father='sidebar' specialBehaviour="innerLink" bordered={true} />);
        expect(wrapper).toMatchSnapshot();
    });

    for (let i = 0; i < numberOfTests; i++) {
        test(`pruebas clase bordeado numero ${i}`, () => {
            let isBordered = (Math.random() < 0.5);
            let wrapper = shallow(<ButtonGeneral key={`sidebar-button-1`} action={actions[0][1]} text={actions[0][0]} father='sidebar' specialBehaviour="innerLink" bordered={isBordered} />);
            expect(wrapper.find('.bordered').isEmptyRender()).toBe(!isBordered);
        });
    }

    for (let i = 0; i < numberOfTests; i++) {
        test(`pruebas link interno numero ${i}`, () => {
            let isInternLink = (Math.random() < 0.5);
            let wrapper = shallow(<ButtonGeneral key={`sidebar-button-1`} action={actions[0][1]} text={actions[0][0]} father='sidebar' specialBehaviour={isInternLink ? "innerLink" : ''} bordered={false} />);
            expect(wrapper.find('NavLink').isEmptyRender()).toBe(!isInternLink);
        });
    }

})