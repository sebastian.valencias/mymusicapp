import { shallow } from 'enzyme';
import '@testing-library/jest-dom';
import ButtonSpotifyListElement from '../../../../components/molecules/spotify/ButtonSpotifyListElement';


describe('ButtonSpotifySong', () => {
    //action, bordered = false, text, father, buttonType = "standar", specialBehaviour
    const params = [{ description: 'album 1', id: 1, image: 'image/happy.jpg', name: 'song 1' },
    { album: 'album 2', id: 'asdasd1234', image: 'image/happy.jpg', name: 'song 2' },
    { album: 'album 3', id: '233ffgwwfff', image: 'image/happy.jpg', name: 'song 3' },
    { album: 'album 4', id: 50, image: 'image/happy.jpg', name: 'song 4' }
    ]

    //key={id} image={image} description={description} idElement={id} name={name}

    test('prueba renderizado', () => {
        let wrapper = shallow(<ButtonSpotifyListElement key={params[0].id} idElement={params[0].id} image={params[0].image} name={params[0].name} />);
        expect(wrapper).toMatchSnapshot();
    });

    for (let i = 0; i < params.length; i++) {
        test('prueba contenido', () => {
            let wrapper = shallow(<ButtonSpotifyListElement key={params[i].id} idElement={params[i].id} image={params[i].image} name={params[i].name} />);
            expect(wrapper.find('img').prop('src')).toEqual(params[i].image);
            expect(wrapper.find('.button-song-spotify__song-name_artist h4').text()).toEqual(params[i].name);
            expect(wrapper.find('.button-song-spotify__song-name_artist p').text()).toEqual(params[i].artist);
            expect(wrapper.find('.button-song-spotify__song-album p').text()).toEqual(params[i].album);
            //tiempo
            expect(wrapper.find('.button-song-spotify__song-save_duration h4').text()).toEqual(params[i].durationHH_MM_SS);
        });
    }
    //puse todas en el mismo punto porque seria una bobada re renderizar todos los elementos de 
    //ese arreglo para hacer una consulta relativamente similar y una evaluacion practicamente identica
});