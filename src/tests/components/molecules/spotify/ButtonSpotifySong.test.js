import { shallow } from 'enzyme';
import '@testing-library/jest-dom';
import ButtonSpotifySong from '../../../../components/molecules/spotify/ButtonSpotifySong';


describe('ButtonSpotifySong', () => {
    //action, bordered = false, text, father, buttonType = "standar", specialBehaviour
    const params = [{ album: 'album 1', id: 1, image: 'image/happy.jpg', name: 'song 1', artist: 'Mr and Miss singer', duration: 150000, durationHH_MM_SS: '00:02:30', saved: false },
    { album: 'album 2', id: 'asdasd1234', image: 'image/happy.jpg', name: 'song 2', artist: 'Mr and Miss singer 76646', duration: 5480000, durationHH_MM_SS: '01:31:20', saved: true },
    { album: 'album 3', id: '233ffgwwfff', image: 'image/happy.jpg', name: 'song 3', artist: 'Mr and Miss singer 1234', duration: 480000, durationHH_MM_SS: '00:08:00', saved: true },
    { album: 'album 4', id: 50, image: 'image/happy.jpg', name: 'song 4', artist: 'Mr and Miss singer 84654', duration: 989846, durationHH_MM_SS: '00:16:29', saved: false }
    ]

    test('prueba renderizado', () => {
        let wrapper = shallow(<ButtonSpotifySong key={params[0].id} idElement={params[0].id} image={params[0].image} album={params[0].album} name={params[0].name} artist={params[0].artist} duration={params[0].duration} saved={params[0].saved} />);
        expect(wrapper).toMatchSnapshot();
    });
    for (let i = 0; i < params.length; i++) {
        test('prueba contenido', () => {
            let wrapper = shallow(<ButtonSpotifySong key={params[i].id} idElement={params[i].id} image={params[i].image} album={params[i].album} name={params[i].name} artist={params[i].artist} duration={params[i].duration} saved={params[i].saved} />);
            expect(wrapper.find('img').prop('src')).toEqual(params[i].image);
            expect(wrapper.find('.button-song-spotify__song-name_artist h4').text()).toEqual(params[i].name);
            expect(wrapper.find('.button-song-spotify__song-name_artist p').text()).toEqual(params[i].artist);
            expect(wrapper.find('.button-song-spotify__song-album p').text()).toEqual(params[i].album);
            //tiempo
            expect(wrapper.find('.button-song-spotify__song-save_duration h4').text()).toEqual(params[i].durationHH_MM_SS);
        });
    }
    //puse todas en el mismo punto porque seria una bobada re renderizar todos los elementos de 
    //ese arreglo para hacer una consulta relativamente similar y una evaluacion practicamente identica
});