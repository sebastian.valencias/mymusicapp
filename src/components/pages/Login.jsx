import React from 'react'
import ButtonGeneral from '../atoms/buttons/ButtonGeneral'
import { loginHandler } from '../../handlers/HTTPHandlers'

import './_Login.scss';

import MyMusic from '../../assets/images/MyMusic.png'

function Login() {
    return (
        <>
            <main className='block-login'>
                <section>
                    <figure><img className='image-fluid' src={MyMusic} alt="cosa" /></figure>
                    <h2>My Music App</h2>
                    <button onClick={loginHandler}>Ingresar</button>
                </section>
            </main>


        </>
    )
}

export default Login
