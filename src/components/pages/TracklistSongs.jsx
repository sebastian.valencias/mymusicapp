import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import ListSongs from '../organisms/ListSongs';
import { getListHandler } from '../../handlers/HTTPHandlers';

function TracklistSongs() {
    const params = useParams();

    const [songList, setSongList] = useState([])
    console.log(params.idList, "ID  DE LA LISTA")
    useEffect(() => {
        getListHandler(setSongList, params.idList);
    }, []);
    return (
        <>
            <section className='flex-list-mutable-on-width section-spotify'>
                <ListSongs elements={songList} />
            </section>
        </>
    );
}

export default TracklistSongs;
