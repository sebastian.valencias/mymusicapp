import React, { useEffect, useState } from 'react'
import { BrowseListsHandlerd } from '../../handlers/HTTPHandlers';
import ListPlaylists from '../organisms/ListPlaylists';

import './_Home.scss';

const Home = () => {
    const [browseLists, setBrowseLists] = useState([])


    useEffect(() => {
        BrowseListsHandlerd(setBrowseLists);
    }, []);
    const playlistsProps = browseLists.map((element) => {
        const elementListProperties = {
            description: element.description,
            id: element.id,
            name: element.name,
            image: element.images[0].url
        }
        return elementListProperties;
    })
    return (

        <>
            <section className='flex-list-mutable-on-width section-spotify'>
                <ListPlaylists elements={playlistsProps} />
            </section>
        </>

    );
}

export default Home;