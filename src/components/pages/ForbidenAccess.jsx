import React from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

function ForbidenAccess() {
  const navigate = useNavigate();
  const state = useSelector(state => state);
  if (state.auth.expiresIn !== null) {
    navigate('/', { replace: true })
  } else {
    navigate('/login', { replace: true })
  }
  return <div></div>;
}

export default ForbidenAccess;
