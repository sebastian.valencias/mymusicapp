import React from 'react';
import { useState, useEffect } from 'react';
import { getUserListsHandler } from '../../handlers/HTTPHandlers';
import ListPlaylists from '../organisms/ListPlaylists';

function Tracklists() {
    const [tracklists, setTracklists] = useState([])


    useEffect(() => {
        getUserListsHandler(setTracklists);
    }, []);
    console.log(tracklists);
    const playlistsProps = tracklists.map((element) => {
        const elementListProperties = {
            description: element.description,
            id: element.id,
            name: element.name,
            image: element.images[0]?.url
        }
        return elementListProperties;
    })
    return (

        <>
            <section className='flex-list-mutable-on-width section-spotify'>
                <ListPlaylists elements={playlistsProps} />
            </section>
        </>

    );

}

export default Tracklists;
