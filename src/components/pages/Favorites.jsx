import React from 'react';
import { useState, useEffect } from 'react';
import { favoritesHandler } from '../../handlers/HTTPHandlers';
import ListSongs from '../organisms/ListSongs';

function Favorites() {
    const [favList, setFavLists] = useState([])

    useEffect(() => {
        favoritesHandler().then(({ data }) => {
            const list = data.items;
            setFavLists(list);
        });
    }, []);


    const playlistsProps = favList.map((element) => {
        const { track } = element;
        const elementListProperties = {
            album: track.album.name,
            id: track.id,
            duration: track.duration_ms,
            name: track.name,
            image: track.album.images[0].url,
            saved: true,
            artist: (track.artists.map((trackartist) => {
                return trackartist.name;
            })).join(', '),

        }
        return elementListProperties;
    })
    return (<>
        <section className='flex-list-mutable-on-width section-spotify'>
            <ListSongs elements={playlistsProps} />
        </section>

    </>);
}

export default Favorites;
