import React from 'react';
import ButtonGeneral from '../atoms/buttons/ButtonGeneral';

import './_Header.scss';

function Header() {
    const actionOpen = { actionId: 'replace-class', selector: 'aside.sidebar', oldClass: 'hidden', newClass: 'visible' };
    return (
        <header className='main-header'>

            <ButtonGeneral action={actionOpen} text='ABRIR' father='open-sidebar header' buttonType="half-width-centered" />
        </header>
    );
}

export default Header;
