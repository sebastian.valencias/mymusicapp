import React from 'react';
import ButtonSpotifySong from '../molecules/spotify/ButtonSpotifySong';

function ListSongs({ elements }) {
    return (
        <>
            {
                elements.map((element) => {
                    const { album, id, image, name, artist, duration, saved } = element;
                    return <ButtonSpotifySong key={id} idElement={id} image={image} album={album} name={name} artist={artist} duration={duration} saved={saved} />
                })
            }
        </>
    );
}

export default ListSongs;
