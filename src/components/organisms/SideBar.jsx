import React from 'react'
import AccountActions from '../molecules/sideBar/AccountActions'
import SidebarActions from '../molecules/sideBar/SidebarActions'
import ButtonGeneral from '../atoms/buttons/ButtonGeneral'

//styles
import './_SideBar.scss'

const SideBar = ({ logedUser }) => {
    const { name, image } = logedUser;
    const actionClose = { actionId: 'replace-class', selector: 'aside.sidebar', oldClass: 'visible', newClass: 'hidden' }
    return (

        <aside className='sidebar hidden'>
            <ButtonGeneral className="ahhhh" action={actionClose} text='X' father='close-button sidebar' />
            <AccountActions userName={name} userImage={image} father="sidebar" />
            <SidebarActions />
        </aside>
    );
}

export default SideBar;