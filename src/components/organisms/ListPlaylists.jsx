import React from 'react';
import ButtonSpotifyListElement from '../molecules/spotify/ButtonSpotifyListElement';

import './_ListPlaylists.scss';

function ListPlaylists({ elements }) {
    return <>
        {
            elements.map((element) => {
                const { description, id, image, name } = element;
                return <ButtonSpotifyListElement key={id} image={image} description={description} idElement={id} name={name} />
            })
        }
    </>;
}

export default ListPlaylists;
