import React from 'react';
import ButtonGeneral from '../../atoms/buttons/ButtonGeneral';


function SidebarActions() {
    const actions = [
        ['Inicio', { actionId: 'redirect', destiny: '/' }],
        ['Favoritos', { actionId: 'redirect', destiny: '/fav' }],
        ['Mis listas', { actionId: 'redirect', destiny: '/lists' }],

    ]
    const actionClose = { actionId: 'replace-class', selector: 'aside.sidebar', oldClass: 'visible', newClass: 'hidden' }
    //return (<div>H2 holiiii</div>)

    return (
        <section>
            {
                actions.map((action) => {
                    return <ButtonGeneral key={`sidebar-button-${action[1].destiny.split('/')[1]}`} action={action[1]} text={action[0]} father='sidebar' specialBehaviour="innerLink" bordered={true} />
                })
            }
        </section>
    );
}

export default SidebarActions;
