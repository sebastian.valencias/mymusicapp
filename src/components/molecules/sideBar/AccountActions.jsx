import React from 'react'
import ButtonGeneral from '../../atoms/buttons/ButtonGeneral'
import UserImage from '../../atoms/images/UserImage'


//style
import './_AccountActions.scss';

function AccountActions({ userName, userImage, father }) {
    const action = { actionId: 'log-out' }
    return (
        <section className={`${father}__show-account`}>
            <UserImage src={userImage} alter={userName} />
            <ButtonGeneral action={action} text='Cerrar sesión' father={father} ></ButtonGeneral>
            <hr />
        </section>
    )
}

export default AccountActions
