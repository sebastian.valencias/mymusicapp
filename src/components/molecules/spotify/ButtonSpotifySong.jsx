import React from 'react';
import ButtonLike from '../../atoms/buttons/ButtonLike';

import './_ButtonSpotifySong.scss';

function ButtonSpotifySong({ idElement, name, album, image, artist, duration, saved }) {
    return (
        <button className='button-song-spotify'>
            <figure>
                <img className="image-fluid" src={image} alt={`image ${name}`} />
            </figure>
            <div className='button-song-spotify__song-name_artist'>
                <h4>{name}</h4>
                <p>{artist}</p>
            </div>
            <div className='button-song-spotify__song-album'>
                <p>{album}</p>
            </div>
            <div className='button-song-spotify__song-save_duration'>
                <ButtonLike idElement={idElement} saved={saved} />
                <h4>{milisecondsToTime(duration).join(':')}</h4>
            </div>
        </button>
    );
}

const milisecondsToTime = (duration) => {

    let time = [Math.floor((duration / (1000 * 60 * 60)) % 24),
    Math.floor((duration / (1000 * 60)) % 60),
    Math.floor((duration / 1000) % 60)];
    time = time.map((element) => {
        return element > 9 ? element : `0${element}`;
    })
    return time
}

export default ButtonSpotifySong;
