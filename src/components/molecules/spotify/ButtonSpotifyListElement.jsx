import React from 'react';
import ButtonGeneral from '../../atoms/buttons/ButtonGeneral';

import './_ButtonSpotifyListElement.scss';

function ButtonSpotifyListElement({ image, description, idElement, name }) {
    const buttonAction = {
        actionId: "redirect",
        destiny: `/lists/${idElement}`
    }
    return (
        <button className={`button-spotify-element`}>
            <figure className={`button-spotify-element__image-holder`}>
                <img className='image-fluid' src={image} alt={`image playlist ${name}`} />
            </figure>
            <h3>{name}</h3>
            <h4>{description}</h4>
            <ButtonGeneral action={buttonAction} bordered={true} text={'Ver lista'} father={'button-spotify-element'} buttonType="half-width-centered" specialBehaviour="innerLink" />
        </button>
    );
}
//<ButtonLike idElement={id} />
export default ButtonSpotifyListElement;
