import React from 'react';
import { PropTypes } from 'prop-types';
import { buttonSidebarClick } from '../../../handlers/ButtonHandler';

//styles
import './_ButtonGeneral.scss';
import { Link, NavLink } from 'react-router-dom';

function ButtonGeneral({ action, bordered = false, text, father, buttonType = "standar", specialBehaviour }) {

    const actionClose = { actionId: 'replace-class', selector: 'aside.sidebar', oldClass: 'visible', newClass: 'hidden' }
    if (specialBehaviour == 'innerLink') {
        return (
            <NavLink className={({ isActive }) => `button-link` + (isActive ? ' active' : '')} to={action.destiny}>
                <button className={`${father}__button${bordered ? ' bordered' : ''} button-text--${buttonType}`} onClick={() => buttonSidebarClick(actionClose)}>
                    {text}
                </button>
            </NavLink>
        )
    }
    return (
        <button className={`${father}__button${bordered ? ' bordered' : ''} button-text--${buttonType}`} onClick={() => buttonSidebarClick(action)}>
            {text}
        </button>
    )
}


export default ButtonGeneral

ButtonGeneral.propTypes = {
    action: PropTypes.object.isRequired,
    bordered: PropTypes.bool,
    text: PropTypes.string.isRequired,
    father: PropTypes.string.isRequired,

}