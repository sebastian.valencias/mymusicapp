import React from 'react';
import { removeSavedSong, saveSong } from '../../../handlers/HTTPHandlers';
import { useState } from 'react';

import './_ButtonLike.scss';


function ButtonLike({ idElement, saved }) {
    const [savedElement, setSavedElement] = useState(saved)
    const likeButtonEvent = (id, setSaved, saved) => {
        if (saved) {
            console.log(id);
            removeSavedSong(id);
            setSaved(false);
        }
        else {
            saveSong(id);
            setSaved(true);
        }
    }

    return <button onClick={() => { likeButtonEvent(idElement, setSavedElement, savedElement); }} className={`button-like${savedElement ? ' saved-song' : ''}`}></button>;
}

export default ButtonLike;
