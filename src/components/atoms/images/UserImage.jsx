import React from 'react'

//style
import './UserImage.scss';

function UserImage({ src, alter, father = 'sidebar' }) {
    return (
        <figure className={`${father}__user-image`}>
            <img className='image-fluid' src={src} alt={"Image profile " + alter} />
            <figcaption>
                Hola {alter}
            </figcaption>
        </figure>
    )
}

export default UserImage
