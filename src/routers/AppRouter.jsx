import React, { useEffect } from 'react';
import { Routes, Route, BrowserRouter, useNavigate } from "react-router-dom";
import RouterAuthenticated from './RouterAuthenticated';
import { sesionExpiration, TokenManager } from '../handlers/SesionHandler';
import Login from '../components/pages/Login'
import { useState } from 'react';

import { useDispatch, useSelector } from 'react-redux';

import { login } from '../actions/auth';

export const AppRouter = () => {

    const dispatch = useDispatch();
    const [loginState, setloginState] = useState(false);
    const state = useSelector(state => state)
    useEffect(() => {
        if (localStorage.getItem('expiresIn') !== null) {
            dispatch(login(localStorage.getItem('accessToken'), localStorage.getItem('expiresIn')));
            setloginState(true);
            const timeToExpire = (localStorage.getItem('expiresIn') - Date.now());
            console.log(timeToExpire);
            sesionExpiration(setloginState, timeToExpire > 0 ? timeToExpire : 5);
        } else {
            setloginState(false);
        }

    }, [loginState])

    return (
        <BrowserRouter>
            <Routes>
                {loginState ? <Route path="/*" element={<RouterAuthenticated />} /> : <Route path="/*" element={<Login />} />}
            </Routes>
        </BrowserRouter>);
};
