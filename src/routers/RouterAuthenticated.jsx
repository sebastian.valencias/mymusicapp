import React, { useEffect } from 'react';
import ButtonGeneral from '../components/atoms/buttons/ButtonGeneral';
import Home from "../components/pages/Home";
import Header from '../components/organisms/Header';
import SideBar from '../components/organisms/SideBar';
import { useState } from 'react'
import { profileDataHandler } from '../handlers/HTTPHandlers';
import { Routes, Route, Link, BrowserRouter } from "react-router-dom";
import Favorites from '../components/pages/Favorites';
import Tracklists from '../components/pages/Tracklists';
import TracklistSongs from '../components/pages/TracklistSongs';
import ForbidenAccess from '../components/pages/ForbidenAccess';

function RouterAuthenticated() {

  const [user, setUser] = useState({})
  useEffect(() => {
    profileDataHandler(setUser);
  }, []);

  return (
    <>

      <SideBar logedUser={user} />
      <main className="main-container-spotify">
        <Header></Header>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/fav" element={<Favorites />} />
          <Route path="/lists" element={<Tracklists />} />
          <Route path="/lists/:idList" element={<TracklistSongs />} />
          <Route path="/*" element={<ForbidenAccess />} />
        </Routes>
      </main>
    </>
  );
}

export default RouterAuthenticated;
